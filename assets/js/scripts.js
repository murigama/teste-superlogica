const API_URL = "https://5fce8ae43e19cc00167c5f42.mockapi.io/api/faq";
const faqListElement = document.querySelector('.faq-list');

async function getFaq() {
    const response = await fetch(API_URL);
    const faq = await response.json();
    
    faq.forEach(element => {
       console.log(element);
       insertFaqElement(element);
    });

    mountFaqAccordion();
}

function insertFaqElement (element) {

    console.log(element.question);

    const listElement = document.createElement('li');
    listElement.classList.add('faq-list__item');

    const listElementQuestion = document.createElement('h2');
    listElementQuestion.classList.add('faq-list__question');
    listElementQuestion.innerText = element.question;
    listElement.appendChild(listElementQuestion);

    const listElementAnswer = document.createElement('p');
    listElementAnswer.classList.add('faq-list__answer');
    listElementAnswer.innerText = element.answer;
    listElement.appendChild(listElementAnswer);

    faqListElement.appendChild(listElement);

}

window.onload = function() {
    getFaq();
}

function mountFaqAccordion() {
    
    const accordions = document.querySelectorAll(".faq-list");
    
    for (const accordion of accordions) {
        const panels = accordion.querySelectorAll(".faq-list__item");
        for (const panel of panels) {
            const head = panel.querySelector(".faq-list__question");
                
            head.addEventListener('click', () => {
                console.log('click');
                for (const otherPanel of panels) {
                    if (otherPanel !== panel) {
                        otherPanel.classList.remove('expanded');
                    }
                }
                panel.classList.toggle('expanded');
            });
        }
    }
}

window.addEventListener('load', function(){
    new Glider(document.querySelector('.plans-cards'), {
        slidesToShow: 1,
        draggable: true,
        dots: ".plans-cards__dots",
        responsive: [
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
              },
            },
          ],
    });

    new Glider(document.querySelector('.features-cards'), {
        slidesToShow: 1,
        draggable: true,
        dots: ".features-cards__dots",
        responsive: [
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
              },
            },
          ],
    });

  })